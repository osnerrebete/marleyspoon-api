# frozen_string_literal: true

class ContentfulService
  SPACE_ID = ENV['CONTENTFUL_SPACE_ID']
  ENV_ID = ENV['CONTENTFUL_ACCESS_ENV']
  ACCESS_TOKEN = ENV['CONTENTFUL_ACCESS_TOKEN']

  def self.call(*args, &block)
    new(*args, &block).call
  end

  def call_entries_api
    request = Typhoeus::Request.new(url, followlocation: true)
    response = request.run

    handle_response(response)
  end

  def url; end

  def entries_url
    "https://cdn.contentful.com/spaces/#{SPACE_ID}/environments/#{ENV_ID}/entries?access_token=#{ACCESS_TOKEN}"
  end

  def handle_errors(response)
    if response.timed_out?
      Rails.logger.error('got a time out')
    elsif response.code.zero?
      Rails.logger.error(response.return_message)
    else
      Rails.logger.error("HTTP request failed: #{response.code}")
    end
  end

  def handle_response(response)
    result = nil
    if response.success?
      result = JSON.parse(response.body)
    else
      handle_errors(response)
    end

    result
  end
end
