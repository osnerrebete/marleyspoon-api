# frozen_string_literal: true

module Recipes
  class Fetch < ContentfulService
    CONTENT_TYPE_ID = 'recipe'
    FILTER_BY_FIELDS = 'sys.id,fields.title,fields.photo'

    def call
      entries_hash
    end

    private

    def entries_hash
      entries = call_entries_api
      return {} if entries.blank? || entries['items'].blank?

      entries_items = entries['items']
      entries_assets = entries['includes']['Asset']
      build_entries_hash(entries_assets, entries_items)
    end

    def build_entries_hash(entries_assets, entries_items)
      entries_items.map do |entry|
        photo_id = entry['fields']['photo']['sys']['id']
        photo = entries_assets.extract! { |asset| asset['sys']['id'] == photo_id }
        photo_url = photo[0]['fields']['file']['url']

        {
          id: entry['sys']['id'],
          title: entry['fields']['title'],
          photo_url: photo_url
        }
      end
    end

    def url
      entries_url + "&content_type=#{CONTENT_TYPE_ID}&select=#{FILTER_BY_FIELDS}"
    end
  end
end
