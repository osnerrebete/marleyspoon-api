# frozen_string_literal: true

module Recipes
  class Show < ContentfulService
    CONTENT_TYPE_ID = 'recipe'
    FILTER_BY_FIELDS = 'sys.id,fields.title,fields.photo'

    attr_reader :entry_id

    def initialize(entry_id:)
      @entry_id = entry_id
    end

    def call
      build_recipe_hash
    end

    private

    def build_recipe_hash
      entries = call_entries_api
      return {} if entries.blank? || entries['items'].blank?

      entry = entries['items'].first
      photos = entries['includes']['Asset']
      other_entries = entries['includes']['Entry']
      build_entries_hash(entry, photos, other_entries)
    end

    def build_entries_hash(entry, photos, other_entries)
      photo_id = entry['fields']['photo']['sys']['id']
      tags, chefs = formatted_tags_and_chef(other_entries)

      {
        id: entry['sys']['id'],
        title: entry['fields']['title'],
        description: entry['fields']['description'],
        photo_url: find_photo_url(photos, photo_id),
        tags: tags,
        chefs: chefs
      }
    end

    def find_photo_url(photos, photo_id)
      photo = photos.extract! { |asset| asset['sys']['id'] == photo_id }

      photo[0]['fields']['file']['url']
    end

    def formatted_tags_and_chef(other_entries)
      tags = []
      chefs = []

      other_entries&.each do |entry|
        case entry['sys']['contentType']['sys']['id']
        when 'tag'
          tags << build_other_entries_hash(entry)
        when 'chef'
          chefs << build_other_entries_hash(entry)
        end
      end

      [tags, chefs]
    end

    def build_other_entries_hash(entry)
      {
        id: entry['sys']['id'],
        name: entry['fields']['name']
      }
    end

    def url
      entries_url + "&content_type=#{CONTENT_TYPE_ID}&sys.id=#{@entry_id}"
    end
  end
end
