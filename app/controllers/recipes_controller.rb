class RecipesController < ApplicationController
  def index
    recipes = Recipes::Fetch.call

    render json: recipes
  end

  def show
    recipe = Recipes::Show.call(entry_id: params[:id])

    render json: recipe
  end
end
