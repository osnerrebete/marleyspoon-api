require 'rails_helper'

RSpec.shared_context 'mock_ok_request' do
  before do
    mocked_response = File.new(Rails.root.join('test/fixtures/webmock/recipes.json'))

    stub_request(:get, /entries/)
      .to_return(status: 200, body: mocked_response, headers: {})
  end
end

RSpec.shared_context 'mock_timeout_request' do
  before do
    stub_request(:get, /entries/)
      .to_timeout
  end
end

RSpec.shared_context 'mock_error_request' do
  before do
    stub_request(:get, /entries/)
      .to_return(status: 404, body: nil, headers: {})
  end
end

RSpec.describe 'Recipes', type: :request do
  describe 'GET /recipes' do
    context 'when the API response is OK' do
      include_context 'mock_ok_request'

      it 'lists the recipes' do
        headers = { 'ACCEPT' => 'application/json' }
        get '/recipes', headers: headers
        expected_response = [
          { 'id' => '1', 'title' => 'Recipe Title',
            'photo_url' => '//img.jpg' },
          { 'id' => '2', 'title' => 'Recipe Title 2',
            'photo_url' => '//img2.jpg' }]

        expect(response.content_type).to eq('application/json; charset=utf-8')
        expect(response).to have_http_status(:ok)
        expect(JSON.parse(response.body)).to eq(expected_response)
      end
    end

    context 'when the API response takes too long' do
      include_context 'mock_timeout_request'

      it 'lists an empty hash' do
        headers = { 'ACCEPT' => 'application/json' }
        get '/recipes', headers: headers
        expected_response = {}

        expect(response.content_type).to eq('application/json; charset=utf-8')
        expect(response).to have_http_status(:ok)
        expect(JSON.parse(response.body)).to eq(expected_response)
      end
    end

    context 'when the API response is an error' do
      include_context 'mock_error_request'

      it 'lists an empty hash' do
        headers = { 'ACCEPT' => 'application/json' }
        get '/recipes', headers: headers
        expected_response = {}

        expect(response.content_type).to eq('application/json; charset=utf-8')
        expect(response).to have_http_status(:ok)
        expect(JSON.parse(response.body)).to eq(expected_response)
      end
    end
  end

  describe 'GET /recipes/:id' do
    context 'when the id is present and the API response is OK' do
      include_context 'mock_ok_request'

      it 'returns the recipe' do
        headers = { 'ACCEPT' => 'application/json' }
        recipe_id = '1234'
        get "/recipes/#{recipe_id}", headers: headers
        expected_response = { 'id' => '1', 'photo_url' => '//img.jpg',
                              'chefs' => [{ 'id' => '1', 'name' => 'Mark Zucchiniberg ' },
                                          { 'id' => '4', 'name' => 'Jony Chives' }],
                              'description' => 'Description',
                              'tags' => [{ 'id' => '2', 'name' => 'vegan' },
                                         { 'id' => '3', 'name' => 'gluten free' },
                                         { 'id' => '5', 'name' => 'healthy' }],
                              'title' => 'Recipe Title' }

        expect(response.content_type).to eq('application/json; charset=utf-8')
        expect(response).to have_http_status(:ok)
        expect(JSON.parse(response.body)).to eq(expected_response)
      end
    end

    context 'when the API response takes too long' do
      include_context 'mock_timeout_request'

      it 'returns an empty hash' do
        headers = { 'ACCEPT' => 'application/json' }
        recipe_id = '1234'
        get "/recipes/#{recipe_id}", headers: headers
        expected_response = {}

        expect(response.content_type).to eq('application/json; charset=utf-8')
        expect(response).to have_http_status(:ok)
        expect(JSON.parse(response.body)).to eq(expected_response)
      end
    end

    context 'when the API response is an error' do
      include_context 'mock_error_request'

      it 'returns an empty hash' do
        headers = { 'ACCEPT' => 'application/json' }
        recipe_id = '1234'
        get "/recipes/#{recipe_id}", headers: headers
        expected_response = {}

        expect(response.content_type).to eq('application/json; charset=utf-8')
        expect(response).to have_http_status(:ok)
        expect(JSON.parse(response.body)).to eq(expected_response)
      end
    end
  end
end
