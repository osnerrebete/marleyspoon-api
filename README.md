# README

**About the application and dependencies:**

* Ruby version: ruby-2.6.6 / Ruby on Rails: 6

* Configuration: there's an _.env_ file that allows to set the enviroment variables, it shouldn't have passwords but it has them right now to allow to test the app faster

* How to run the test suite: with the command _rspec_ from a terminal

* Run server:
It's necessary to use the _rails s_ command to run the application.
